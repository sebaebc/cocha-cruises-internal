import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FormViewComponent } from './Components/form-view/form-view.component';

const routes: Routes = [
  { path: '', redirectTo: '/detail', pathMatch: 'full' },
  { path: 'form', component: FormViewComponent },
  { path: 'form/:idCruise', component: FormViewComponent },
  { path: 'form/:idCruise/:departure', component: FormViewComponent },
  { path: 'form/:idCruise/:departure/:arrival', component: FormViewComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
