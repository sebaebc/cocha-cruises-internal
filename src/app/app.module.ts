import { BrowserModule } from '@angular/platform-browser';

import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FormViewComponent } from './Components/form-view/form-view.component';
import { AppRoutingModule } from './app-routing.module';

// Cocha External
// import { CochaComponentsModule } from '@cocha/cocha-components';
// import { FetchModule } from '@cocha/cocha-fetch';

import { NgxPageScrollModule } from 'ngx-page-scroll';
import { NgxCarouselModule } from 'ngx-carousel';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import 'hammerjs';

// import { MatButtonModule } from '@angular/material';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    FormViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxCarouselModule,
    NgxPageScrollModule,
    NgbModule.forRoot()
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
